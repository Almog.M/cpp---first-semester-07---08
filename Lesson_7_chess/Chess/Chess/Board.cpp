#include "Board.h"
#include <exception>
#include "Pawn.h"
#include "King.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "Point.h"
#include <string>
#include "Tool.h"

using std::string;
using std::exception;
using std::cout; using std::endl;


void Board::swappTools(Point p1, Point p2)
{
	
	Tool* temp = this->_board[p1.getX()][p1.getY()];
	this->_board[p1.getX()][p1.getY()] = this->_board[p2.getX()][p2.getY()];
	this->_board[p2.getX()][p2.getY()] = temp;
}

void Board::initBoardArray(string initPlayString)
{
	int initStringIndex = strlen(initPlayString.c_str()) - 2; //One before the last index, 'cause the last index is for the who the turn is.
	int i = 0, j = 0; //Of the board.

	//Inits the board - We need to init the board in an upside down way according to the init string.
	for (i = 0; i < BOARD_SIZE; i++) //On lines
	{
		for (j = 0; j < BOARD_SIZE; j++) //On column.
		{
			switch (initPlayString[initStringIndex])
			{
			case 'p':
			case 'P':
				this->_board[i][j] = new Pawn(initPlayString[initStringIndex]);
				break;
			case 'r':
			case 'R':
				this->_board[i][j] = new Rook(initPlayString[initStringIndex]);
				break;
			case 'n':
			case 'N':
				this->_board[i][j] = new Knight(initPlayString[initStringIndex]);
				break;

			case 'b':
			case 'B':
				this->_board[i][j] = new Bishop(initPlayString[initStringIndex]);
				break;

			case 'k':
			case 'K':
				this->_board[i][j] = new King(initPlayString[initStringIndex]);
				break;

			case 'q':
			case 'Q':
				this->_board[i][j] = new Queen(initPlayString[initStringIndex]);
				break;

			case '#': //An empty square.
				this->_board[i][j] = nullptr;

				break;
			default:
				throw exception("One of the chars is a non defined char...");
			}
			initStringIndex--;
		}
	}

}

void Board::createBoardArr()
{
	this->_board = new Tool**[_rowLen];
	for (int i = 0; i < this->_rowLen; i++)
		this->_board[i] = new Tool*[this->_colLen];
}

Point Board::getWhiteKingPoint()
{
	return this->_whiteKingPoint;
}

Point Board::getBlakKingPoint()
{
	return this->_blackKingPoint;
}

void Board::setWhiteKingPoint(Point p)
{
	this->_whiteKingPoint = p;
}

void Board::setBlackKingPoint(Point p)
{
	this->_blackKingPoint = p;
}

Tool*** Board::getBoard()
{
	
	return this->_board;
}

int Board::getMaxRowIndex() const
{
	return this->_rowLen - 1;
}

int Board::getMaxColIndex() const
{
	return this->_colLen - 1;
}

bool Board::isValidBoardPoint(Point p)
{
	return !((p.getX() > this->getMaxRowIndex() || p.getX() < 0) || (p.getY() > this->getMaxColIndex() || p.getY() < 0));
}

Board::Board(std::string initPlayString, int _rowLen, int _colLen)
{
	if (strlen(initPlayString.c_str()) == 65) //Right len.
	{
		this->_rowLen = _rowLen;
		this->_colLen = _colLen;
		this->_goBack = nullptr;
		this->setWhiteKingPoint(Point(FIRST_LINE_INDEX, START_KING_COL_INDEX));
		this->setBlackKingPoint(Point(LAST_LINE_INDEX, START_KING_COL_INDEX));

		createBoardArr();
		initBoardArray(initPlayString);

		//Finishing the init of the board.
		swappTools(Point(FIRST_LINE_INDEX,3), Point(FIRST_LINE_INDEX,4)); //Swapping between the king and queen in first line.
		swappTools(Point(LAST_LINE_INDEX,3), Point(LAST_LINE_INDEX,4)); //Swapping between the king and queen in last line.
	}
	else
		throw exception("The initialization play string is not in the right length...");
}

void Board::printBoard()
{
	int i = 0, j = 0;

	cout << "Rows number: " << this->_rowLen << endl;
	cout << "Columns number: " << this->_colLen << "\n";

	for (i = this->_rowLen - 1; i >= 0; i--)
	{
		for (j = this->_colLen - 1; j >= 0; j--)
		{
			if(this->_board[i][j]) //If has a tool here.
				cout << this->_board[i][j]->getType() << ", ";
			else
				cout << '#' << ", ";
		}
		cout << std::endl;
	}
	cout << "\n\n";

	if (this->_goBack)
		cout << "A tool to go back is: " << this->_goBack->getType() << "\n\n";
	else
		cout << "There is not a tool to get back..." << "\n\n";

	cout << "The white king point: " << "\n";
	this->_whiteKingPoint.PrintPoint();
	
	cout << "The black king point: " << "\n";
	this->_blackKingPoint.PrintPoint();
	
}


Board::~Board()
{
	for (int i = 0; i < this->_rowLen; i++)
	{
		for (int j = 0; j < this->_colLen; j++) 
		{
			delete this->_board[i][j];
			this->_board[i][j] = nullptr; //For secure.
		}
		delete[] this->_board[i];
	}

	for (int i = 0; i < this->_rowLen; i++) //For secure.
	{
		this->_board[i] = nullptr;
	}
	delete[] this->_board;
	this->_board = nullptr; //For secure.
}
