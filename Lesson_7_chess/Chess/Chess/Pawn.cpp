#include "Pawn.h"






int Pawn::move(Point src, Point dest, Board * b)
{
	Tool*** s = b->getBoard();
	int type = s[dest.getX()][dest.getY()] == nullptr ? (src.getX() == dest.getX() ? (this->isBlackCheck() ? (src.getY() - 1 == dest.getY() ? 1 : 0) : (src.getY() + 1 == dest.getY() ? 2 : 0)) : 0) : (this->isBlackCheck() ? (src.getX() - 1 == dest.getX() && src.getY() - 1 == dest.getY() ? 3 : (src.getX() + 1 == dest.getX() && src.getY() - 1 == dest.getY() ? 4 : 0)) : (src.getX() - 1 == dest.getX() && src.getY() + 1 == dest.getY() ? 5 : (src.getX() + 1 == dest.getX() && src.getY() + 1 == dest.getY() ? 6 : 0)));
	if (type) return 6;
	return 0;
}

Pawn::Pawn(char type) : Tool(type)
{
	_turn = 0;
}

Pawn::~Pawn()
{
}
