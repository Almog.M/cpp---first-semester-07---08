#include "Player.h"
#include "Board.h"
#include "Tool.h"

bool Player::_isWhiteTurn = strlen(INIT_BOARD_MSG) == 65 && INIT_BOARD_MSG[64] == '0'; //If the msg is in a valid len and the last char is a zero.

bool Player::isItCurrPlayerTool(Point p, Board * b)
{
	Tool* toolInPoint = b->getBoard()[p.getX()][p.getY()];
	bool isWhitePlayerAndWhiteTool = toolInPoint && Player::_isWhiteTurn && !toolInPoint->isBlackCheck(); //If this is the white turn and the tool is with a big letter symbol.
	bool isBlackPlayerAndBlackTool = toolInPoint && !Player::_isWhiteTurn && toolInPoint->isBlackCheck(); //If this is the black turn and the tool is with a small letter symbol.
	return isWhitePlayerAndWhiteTool || isBlackPlayerAndBlackTool;
}

bool Player::isWhiteTurn()
{
	return Player::_isWhiteTurn;
}

void Player::setBoolWhiteTurn(bool isItWhiteTurn)
{
	Player::_isWhiteTurn = isItWhiteTurn;
}

Player::Player()
{
}


Player::~Player()
{
}
