#pragma once
#include "Tool.h"
class Queen :
	public Tool
{
public:
	virtual bool isPotentialMove(Point src, Point dest);
	virtual int move(Point src, Point dest, Board* b);
	Queen(char type);
	virtual ~Queen();
};

