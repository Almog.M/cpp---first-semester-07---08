#pragma once
#include <string>
#include "Tool.h"
#include "Point.h"

using std::string;
#define BOARD_SIZE 8
#define START_LINE_INDEX 0
#define END_LINE_INDEX 7
#define FIRST_LINE_INDEX 0
#define LAST_LINE_INDEX 7
#define START_KING_COL_INDEX 3
#define INIT_BOARD_MSG "rnbkqbnrpppppppp################################PPPPPPPPRNBKQBNR0"

class Tool;

class Board
{
private:
		int _rowLen;
		int _colLen;
		Tool*** _board;
		Tool* _goBack; //Tool that we need to restore in some situations.
		Point _whiteKingPoint;
		Point _blackKingPoint;

		void swappTools(Point p1, Point p2); //Swapping between two tools - helps to the constructor.
		void initBoardArray(std::string initPlayString); //For the constructor function.
		void createBoardArr();//For doing new board array.
public:
	//Getters:
	Point getWhiteKingPoint();
	Point getBlakKingPoint();
	Tool*** getBoard();
	int getMaxRowIndex() const;
	int getMaxColIndex() const;

	//Setters:
	void setWhiteKingPoint(Point p);
	void setBlackKingPoint(Point p);

	bool isValidBoardPoint(Point p);
	Board(std::string initPlayString, int _rowLen = BOARD_SIZE, int _colLen = BOARD_SIZE);
	void printBoard(); //For debugging.
	~Board();
};

