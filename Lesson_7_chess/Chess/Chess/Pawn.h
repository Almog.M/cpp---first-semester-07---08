#pragma once
#include "Tool.h"
class Pawn :
	public Tool
{
	int _turn;
public:
	virtual bool isPotentialMove(Point src, Point dest);
	virtual int move(Point src, Point dest, Board* b);
	Pawn(char type);
	virtual ~Pawn();
};

