#pragma once
#include <string>
#include "Point.h"
#include "Board.h"
#include "Status Codes.h"

class Board;

using namespace std;
class Tool
{
protected:
	 char  _type;
public:
	// Getters
	char getType() const;


	bool isBlackCheck() const;
	// Setters

	// Trying to move a player and returns the code of the action. 
	virtual int move(Point src , Point dest, Board* b) = 0;
	virtual bool isPotentialMove(Point src, Point dest) = 0; //Checks if  the tool can move in theory - just according to the valid path of a tool. 

	//C and D
	Tool(char type);
	virtual ~Tool();
}; 
