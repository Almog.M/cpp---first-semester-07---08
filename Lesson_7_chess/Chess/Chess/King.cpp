#include "King.h"
#include "Player.h"





int King::move(Point src, Point dest, Board* b)
{
	int statusCode = 0;
	Tool*** board = b->getBoard();

	if (!(b->isValidBoardPoint(src) && b->isValidBoardPoint(dest)))
		statusCode = INVALID_POINT_INDEXCES;
	else if (src == dest)
		statusCode = SAME_DST_AND_SRC;
	else if (!Player::isItCurrPlayerTool(src, b))
		statusCode = NOT_FOUND_PLAYER_TOOL_IN_SRC;
	else if (Player::isItCurrPlayerTool(dest, b))
		statusCode = CURR_PLAYER_IN_DEST;
	else if (this->isItValidKingMove(src, dest))
	{
		//Moving the king:
		board[dest.getX()][dest.getY()] = board[src.getX()][src.getY()];
		board[src.getX()][src.getY()] = nullptr; //The old place is empty now.
		
		//updates the king point in the board.
		if (Player::isWhiteTurn())
			b->setWhiteKingPoint(dest);
		else //If the black king has moved.
			b->setBlackKingPoint(dest);

	}

	return statusCode;
}

King::King(char type) : Tool(type)
{

}

King::~King()
{
}

bool King::isItValidKingMove(Point src, Point dest)
{
	//If we are moving one place to right\left or up\down or mix of them.
	return abs(src.getX() - dest.getX()) == 1 || abs(src.getY() - dest.getY()) == 1;
}
