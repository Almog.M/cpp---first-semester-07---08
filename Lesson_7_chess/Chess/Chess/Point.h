#pragma once
#include <string>
#include <iostream>

using namespace std;

class Point
{
private:
	int _x; //Line index.
	int _y; //Column index.

	double avgPoint(); //Calc a point's avg.
public:
	bool operator ==(Point p); //If the two fields are equals to the p's fields.
	bool operator !=(Point p); //The same as above just about not equals.
	bool operator >=(Point p); //About the avg between x and y.
	bool operator <=(Point p); //About the avg between x and y.
	bool operator >(Point p); //About the avg between x and y.
	bool operator <(Point p); //About the avg between x and y.

	Point(char x, char y);
	Point(int x, int y);
	Point();
	int getX();
	int getY();
	~Point();
	void PrintPoint();
	
};

