#pragma once
#include "Point.h"
#include "Board.h"

class Player
{
private:
	static bool _isWhiteTurn;
public:
	//Checks if a point in the board have a tool of the current player.
	static bool isItCurrPlayerTool(Point p, Board* b); //Know about who the turn is by the static member below.
	static bool wasThereChess(bool toCheckSelfChess, Point threateningToolPoint, Board* b); //Check if was a chess on a player by the flag that has given.
	static bool wasThereCheckmate(Point threateningToolPoint, Board* b);//Check if was a checkmate on a player by threatening tool point that has given.
	static bool isWhiteTurn();
	static void setBoolWhiteTurn(bool isItWhiteTurn);
	Player();
	~Player();
};


