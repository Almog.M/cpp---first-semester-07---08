#pragma once
#include "Point.h"
#include "Tool.h"
#include "Player.h"
#include "Math.h"



class King :
	public Tool
{
public:
	virtual int move(Point src, Point dest, Board* b);
	King(char type);
	virtual ~King();
protected:
	bool isItValidKingMove(Point src, Point dest);//Checks if a move is a valid king move by points.
};

