#pragma once
#include "Tool.h"
#include "Point.h"

class Bishop :
	public Tool
{
public:
	Bishop(char type);
	virtual ~Bishop();
	virtual int move(Point src, Point dst, Board* b);
};
